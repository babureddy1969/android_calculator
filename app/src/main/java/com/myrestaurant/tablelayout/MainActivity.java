package com.myrestaurant.tablelayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    String numbers = "";
    String operator = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button clear = findViewById(R.id.buttonC);
        Button zero = findViewById(R.id.button0);
        Button one = findViewById(R.id.button1);
        Button two = findViewById(R.id.button2);
        Button three = findViewById(R.id.button3);
        Button four = findViewById(R.id.button4);
        Button five = findViewById(R.id.button5);
        Button six = findViewById(R.id.button6);
        Button seven = findViewById(R.id.button7);
        Button eight = findViewById(R.id.button8);
        Button nine = findViewById(R.id.button9);
        Button add = findViewById(R.id.buttonAdd);
        Button subtract = findViewById(R.id.buttonSubtract);
        Button multiply = findViewById(R.id.buttonMultiply);
        Button divide = findViewById(R.id.buttonDiv);
        Button equals = findViewById(R.id.buttonEquals);
        final TextView result = findViewById(R.id.textViewResult);
        clear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers="";
                result.setText(numbers);
            }
        });
        zero.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "0";
                result.setText(numbers);
            }
        });
        one.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "1";
                result.setText(numbers);
            }
        });
        two.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "2";
                result.setText(numbers);
            }
        });
        three.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "3";
                result.setText(numbers);
            }
        });
        four.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "4";
                result.setText(numbers);
            }
        });
        five.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "5";
                result.setText(numbers);
            }
        });
        six.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "6";
                result.setText(numbers);
            }
        });
        seven.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "7";
                result.setText(numbers);
            }
        });
        eight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "8";
                result.setText(numbers);
            }
        });
        nine.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "9";
                result.setText(numbers);
            }
        });
        add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "+";
                operator = "+";
                result.setText(numbers);
            }
        });
        subtract.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "-";
                operator = "-";
                result.setText(numbers);
            }
        });
        multiply.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "*";
                operator = "*";
                result.setText(numbers);
            }
        });
        divide.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                numbers += "/";
                operator = "/";
                result.setText(numbers);
            }
        });
        equals.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                result.setText(String.valueOf(compute(numbers,operator)));
                numbers="";

            }
        });
    }
    private static int compute(String s, String operator){
        String s1 = s.substring(0,s.indexOf(operator));
        String s2 = s.substring(s.indexOf(operator)+1);
        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);
        switch (operator){
            case "+" :
                return n1+n2;
            case "-" :
                return n1 - n2;
            case "*" :
                return n1 * n2;
            case "/" :
                return n1 / n2;
            default:
                return 0;
        }
    }
}
